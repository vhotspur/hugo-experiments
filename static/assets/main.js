function do_replace(new_url, new_content) {
    $("nav A").removeClass("active");
    $("nav A").each(function(index, elem) {
        if ($(elem).attr("href") == new_url) {
            $(elem).addClass("active");
        }
    });
    $("#replace-me").empty();
    $("#replace-me").append(new_content);
}

$(document).ready(function() {
    $(".tab").click(null, function(data) {
        var tab_url = $(data.currentTarget).data("tab-url");
        var original_url = $(data.currentTarget).attr("href");
        if (!tab_url) {
            return true;
        }
        $.ajax({
            url: tab_url,
        }).done(function(content) {
            do_replace(original_url, content);
            window.history.pushState({
                    url: original_url,
                    content: content
                }, 'Title', original_url);
        });
        $(this).blur();
        return false;
    });
});

window.addEventListener('popstate', function(event) {
    window.location.assign(event.state.url);
    do_replace(event.state.url, event.state.content);
});
